<?php
/**
 * Created by PhpStorm.
 * User: cesarcruz
 * Date: 11/12/18
 * Time: 9:25 PM
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);

require_once "../vendor/autoload.php";

session_start();


//Environments variable
$dotenv = new Dotenv\Dotenv(__DIR__ . '/..');
$dotenv->load();

//Database
use Illuminate\Database\Capsule\Manager as Capsule;
use Aura\Router\RouterContainer;

$capsule = new Capsule;

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => getenv('DB_HOST'),
    'database' => getenv('DB_NAME'),
    'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASS'),
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

$capsule->setAsGlobal();

$capsule->bootEloquent();

//Request
$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

//Routes
$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();

$map->get('index', '/', ['controller' => 'App\Controllers\IndexController', 'action' => 'indexAction', 'auth' => false]);

$map->get('login', '/login', ['controller' => 'App\Controllers\AuthController', 'action' => 'loginAction', 'auth' => false]);
$map->post('auth', '/auth', ['controller' => 'App\Controllers\AuthController', 'action' => 'postLoginAction', 'auth' => false]);
$map->get('logout', '/logout', ['controller' => 'App\Controllers\AuthController', 'action' => 'logoutAction', 'auth' => true]);

$map->get('register', '/register', ['controller' => 'App\Controllers\UsersController', 'action' => 'registerAction', 'auth' => false]);
$map->post('register.user', '/register/user', ['controller' => 'App\Controllers\UsersController', 'action' => 'postRegisterAction', 'auth' => false]);

$map->post('search.user', '/search/users', ['controller' => 'App\Controllers\UsersController', 'action' => 'postSearchUserAction', 'auth' => true]);

$map->get('admin', '/admin', ['controller' => 'App\Controllers\AdminController', 'action' => 'indexAction', 'auth' => true]);

$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);

if (!$route) {
    echo 'No route';
} else {
    $handlerData = $route->handler;
    $controllerName = $handlerData['controller'];
    $actionName = $handlerData['action'];
    $needsAuth = (!empty($handlerData['auth'])) ? $handlerData['auth'] : false;

    $sessionUserId = (!empty($_SESSION['userId'])) ? $_SESSION['userId'] : false;

    if ($needsAuth && !$sessionUserId) {
        $controllerName = 'App\Controllers\AuthController';
        $actionName = 'loginAction';
    } elseif (!$needsAuth && $sessionUserId) {
        $controllerName = 'App\Controllers\AdminController';
        $actionName = 'indexAction';
    }

    $controller = new $controllerName;
    $response = $controller->$actionName($request);

    foreach ($response->getHeaders() as $name => $values) {
        foreach ($values as $value) {
            header(sprintf('%s: %s', $name, $value), false);
        }
    }
    http_response_code($response->getStatusCode());

    echo $response->getBody();

}