<?php
/**
 * Created by PhpStorm.
 * User: cesarcruz
 * Date: 11/13/18
 * Time: 2:06 AM
 */

namespace App\Controllers;


class AdminController extends BaseController
{
    public function indexAction()
    {
        return $this->renderHTML('admin/index.twig');
    }
}