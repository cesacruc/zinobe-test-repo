<?php

namespace App\Controllers;

use App\Models\User;
use App\Helpers\dataHelper;
use Illuminate\Support\Facades\DB;
use Respect\Validation\Validator AS v;
use Respect\Validation\Exceptions\NestedValidationException;

class UsersController extends BaseController
{

    public function registerAction()
    {
        $countryList = dataHelper::getCountryList();

        return $this->renderHTML('users/register.twig', compact('countryList'));
    }

    public function postRegisterAction($request)
    {
        $responseMessage = [];
        $countryList = dataHelper::getCountryList();

        if ($request->getMethod() == 'POST') {

            $postData = $request->getParsedBody();

            $userValidator = v::key('name', v::stringType()->length(3, null))
                ->key('email', v::email())
                ->key('country', v::in($countryList))
                ->key('password', v::alnum()->length(6, null));

            try {
                $userValidator->assert($postData);

                $postData = $request->getParsedBody();
                $user = new User();
                $user->name = $postData['name'];
                $user->email = $postData['email'];
                $user->country = $postData['country'];
                $user->password = password_hash($postData['password'], PASSWORD_DEFAULT);
                $user->save();

                $responseMessage = ['response' => true, 'reason' => 'Usuario creado con éxito!'];

            } catch (NestedValidationException $e) {

                $e->findMessages([
                    'in' => '{{name}} is must be in (<a href="https://datahub.io/core/country-list/r/0.html" target="_blank"> Country list </a>)'
                ]);

                $responseMessage = ['response' => false, 'reason' => $e->getMessages()];
            }

            return $this->renderHTML('users/register.twig', compact('countryList', 'responseMessage'));
        }
    }

    public function postSearchUserAction($request)
    {
        $responseMessage = [];

        if ($request->getMethod() == 'POST') {

            $postData = $request->getParsedBody();

            $userValidator = v::key('search', v::stringType()->length(2, null));

            try {
                $userValidator->assert($postData);

                $users = User::whereRaw('1')
                    ->whereRaw("lower(name) LIKE '%{$postData['search']}%'")
                    ->orWhereRaw("lower(email) LIKE '%{$postData['search']}%'")
                    ->get();

                $responseMessage = ['response' => true, 'reason' => 'Resultado de la busqueda'];

            } catch (NestedValidationException $e) {
                $users = [];
                $responseMessage = ['response' => false, 'reason' => $e->getMessages()];
            }

            return $this->renderHTML('admin/index.twig', compact('users', 'responseMessage'));
        }
    }
}