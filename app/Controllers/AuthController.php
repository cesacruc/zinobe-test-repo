<?php

namespace App\Controllers;

use Zend\Diactoros\Response\RedirectResponse;
use App\Models\User;

class AuthController extends BaseController
{
    public function loginAction()
    {
        return $this->renderHTML('users/login.twig', []);
    }

    public function postLoginAction($request)
    {
        $responseMessage = [];

        if ($request->getMethod() == 'POST') {

            $postData = $request->getParsedBody();
            $user = User::where('email', $postData['email'])->first();

            if ($user) {
                if (password_verify($postData['password'], $user->password)) {
                    $_SESSION['userId'] = $user->id;
                    return new RedirectResponse('/admin');
                } else {
                    $responseMessage = ['response' => false, 'reason' => 'Bad credentials'];
                }
            } else {
                $responseMessage = ['response' => false, 'reason' => 'Bad credentials'];
            }

            return $this->renderHTML('users/login.twig', compact('responseMessage'));
        }
    }

    public function logoutAction()
    {
        unset($_SESSION['userId']);
        return new RedirectResponse('/login');
    }
}