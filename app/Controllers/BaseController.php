<?php
/**
 * Created by PhpStorm.
 * User: cesarcruz
 * Date: 11/12/18
 * Time: 10:20 PM
 */

namespace App\Controllers;

use Twig_Environment;
use Twig_Loader_Filesystem;
use Zend\Diactoros\Response\HtmlResponse;

class BaseController
{

    protected $templateEngine;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem('../views');

        $this->templateEngine = new Twig_Environment($loader, array(
            'debug' => true,
            'cache' => false,
        ));
        $this->templateEngine->addGlobal('session', $_SESSION);
    }

    public function renderHTML($fileName, $data = [])
    {
        return new HtmlResponse($this->templateEngine->render($fileName, $data));
    }
}