<?php
/**
 * Created by PhpStorm.
 * User: cesarcruz
 * Date: 11/12/18
 * Time: 9:56 PM
 */

namespace App\Controllers;


class IndexController extends BaseController
{
    public function indexAction()
    {
        return $this->renderHTML('index.twig');
    }
}