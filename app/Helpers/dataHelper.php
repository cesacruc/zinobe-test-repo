<?php

namespace App\Helpers;


class dataHelper
{
    public static function getCountryList()
    {
        return $countrySource = array_column(json_decode(file_get_contents(realpath('data/country-list.json')), true), 'Name');
    }
}