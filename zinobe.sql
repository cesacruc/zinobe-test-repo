/*
 Navicat Premium Data Transfer

 Source Server         : Dev - MySql 5.7.22
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:33006
 Source Schema         : zinobe

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 13/11/2018 04:00:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'César Camilo Cruz Cáceres', 'cesacruc@gmail.com', 'Colombia', '$2y$10$u3T44rV87VQB//cu09oh0.bMdkxI4zf8mY4Nou3imqwZvNOu84QwS', '2018-11-13 06:37:49', '2018-11-13 06:37:49');
INSERT INTO `users` VALUES (2, 'Kevin Andres Cruz Cáceres', 'kcruz@gmail.com', 'Colombia', '$2y$10$u3T44rV87VQB//cu09oh0.bMdkxI4zf8mY4Nou3imqwZvNOu84QwS', '2018-11-13 06:37:49', '2018-11-13 06:37:49');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
